from __future__ import print_function

from typing import List, Dict
import httplib2
import os
import base64
import datetime
from pathlib import Path

from django.contrib.auth.models import User

from main.models import Provider, Product
from reference.models import Product as ProductReference

from apiclient import discovery
from oauth2client.file import Storage
import xlrd

SCOPES = ['https://www.googleapis.com/auth/gmail.readonly',
          'https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/gmail.send',
          'https://mail.google.com/']

CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'


def get_credentials(user: User):
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    file_name = "gmail-python-quickstart-" + str(user.id) + ".json"
    credential_path = os.path.join(credential_dir,
                                   file_name)
    if not os.path.isfile(credential_path):
        raise SomeException

    store = Storage(credential_path)
    credentials = store.get()
    # if not credentials or credentials.invalid:
    #     flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
    #     flow.user_agent = APPLICATION_NAME
    #     credentials = tools.run(flow, store)
    #     print('Storing credentials to ' + credential_path)
    return credentials


def create_service(user: User):
    credentials = get_credentials(user)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    return service


def get_attachment(service, result, msg, user, provider) -> str:
    for part in msg['payload']['parts']:
        if part['filename']:
            if 'data' in part['body']:
                data = part['body']['data']
            else:
                att_id = part['body']['attachmentId']
                att = service.users().messages().attachments().get(userId='me', messageId=result["id"],
                                                                   id=att_id).execute()
                data = att['data']
            file_data = base64.urlsafe_b64decode(data.encode('UTF-8'))
            f = part['filename'].split(".xls")[0]  # f is filename and e is extension
            filename = "{0}_{1}_{2}.{3}".format(user, provider, f, ".xls")
            path = os.path.expanduser("~/prices/") + filename
            with open(path, 'wb') as f:
                f.write(file_data)
            return path


def get_email_sender(query: str) -> str:
    email = query.split(" ")[0].split(":")[1]
    return email


def read_msgs(user) -> List[Dict[str, str]]:
    # This function download files and returns files_info
    service = create_service(user)
    queries = []
    for provider in Provider.objects.filter(user=user):
        query = "from:{0} is:unread".format(provider.email_sender)
        queries.append(query)
    files_info = []
    for query in queries:
        file_info = {"provider": "", "file_path": ""}
        results = service.users().messages().list(userId='me',
                                                  q=query).execute()
        if results == {'resultSizeEstimate': 0}:
            continue
        result = results["messages"][0]
        msg = service.users().messages().get(userId='me', id=result["id"]).execute()
        thread_id = msg["threadId"]
        email = get_email_sender(query)
        file_info["provider"] = Provider.objects.get(email_sender=email)
        file_info["file_path"] = get_attachment(service, result, msg, user, provider)
        files_info.append(file_info)
        service.users().threads().modify(userId='me', id=thread_id,
                                         body={'removeLabelIds': ["UNREAD"]}).execute()
        for result in results["messages"]:
            service.users().threads().modify(userId='me', id=result["threadId"],
                                             body={'removeLabelIds': ["UNREAD"]}).execute()
    return files_info


def get_msgs(user: User) -> List[Dict[str, str]]:
    files_info = read_msgs(user)
    return files_info


class SomeException(Exception):
    pass


def write_to_log(error: str, path_to_xls: str):
    date = datetime.datetime.now().strftime("%d.%m.%Y [%H:%M:%S]")
    error_string = "{0} {1} {2}".format(date, error, path_to_xls)
    if Path("~/logs/import.log").exists():
        os.system('echo "{0}" > ~/logs/import.log'.format(error_string))
    else:
        os.system('echo "{0}" >> ~/logs/import.log'.format(error_string))


def create_products(user: User, files_info: List[Dict[str, str]]):
    for file_info in files_info:
        Product.objects.all().filter(provider=file_info["provider"]).filter(user=user).delete()
        try:
            rb = xlrd.open_workbook(file_info["file_path"])
        except FileNotFoundError:
            write_to_log("Не найден файл который был отправлен пользователю с id {0} от поставщика {1}" \
                         .format(user.id, file_info["provider"]), file_info["file_path"])
            return
        sheet = rb.sheet_by_index(0)
        for rownum in range(sheet.nrows):
            if rownum == 0:
                continue
            row = sheet.row_values(rownum)
            x = 0
            product = Product()
            product.provider = file_info["provider"]
            product.user = user
            err = False
            for c_el in row:
                if x == 0:
                    try:
                        product.barcode = int(c_el)
                    except ValueError:
                        err = True
                        write_to_log("Ошибка не указан штрихкод (A{0})\n".format(rownum + 1),
                                     file_info["file_path"])
                elif x == 1:
                    try:
                        product.name = str(c_el).upper()
                    except ValueError:
                        err = True
                        write_to_log("Ошибка не указано название (B{0})\n".format(rownum + 1),
                                     file_info["file_path"])
                elif x == 2:
                    try:
                        product.cost = float(c_el)
                    except ValueError:
                        err = True
                        write_to_log("Ошибка не указана стоимость (C{0})\n".format(rownum + 1),
                                     file_info["file_path"])
                elif x == 3:
                    try:
                        product.shelf_life = datetime.datetime.strptime(c_el, "%d.%m.%Y")
                    except ValueError:
                        product.shelf_life = None
                        write_to_log("""Предупреждение не указан срок годности или указан в неверном формате
                                     должен быть в формате
                                     ДД.ММ.ГГ(D{0})\n""".format(rownum + 1),
                                     file_info["file_path"])
                    except TypeError:
                        product.shelf_life = datetime.datetime(*xlrd.xldate_as_tuple(c_el, rb.datemode))
                elif x == 4:
                    try:
                        product.quantity = int(c_el)
                    except ValueError:
                        err = True
                        write_to_log("Ошибка не указано кол-во (E{0})\n".format(rownum + 1),
                                     file_info["file_path"])
                elif x == 5:
                    try:
                        product.multiplicity = int(c_el)
                    except ValueError:
                        product.multiplicity = 1
                        write_to_log("Предупреждение не указана кратность (F{0})".format(rownum + 1),
                                     file_info["file_path"])
                x += 1
            if not err:
                product.save()
            else:
                continue
            create_reference(product)
        path, filename = os.path.split(file_info["file_path"])
        os.system('mv {0} {2}/_read_{1}'.format(file_info["file_path"],
                                                filename,
                                                path))


def create_reference(product: Product):
    if not product.barcode:
        return
    product_reference = ProductReference()
    list_reference = list(ProductReference.objects.all().filter(barcode=product.barcode))
    if not list_reference:
        product_reference.name = product.name
        product_reference.barcode = product.barcode
    elif True in [item.name == product.name for item in list_reference]:
        return
    else:
        product_reference.name = product.name
        product_reference.barcode = product.barcode
    product_reference.save()
