from django.views.generic import View
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse

from main.models import Order, OrderProduct, Product, Provider
from .CommonViews import CommonView

from apiclient import discovery
from oauth2client.file import Storage

import os
import base64
import mimetypes
import httplib2

from typing import List, Dict, Tuple

from email.mime.multipart import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class HistoryView(CommonView):
    model = Order
    template_name = "history.html"

    def get_context_data(self, **kwargs):
        context = super(HistoryView, self).get_context_data(**kwargs)
        provider = self.request.GET.get("provider")
        if provider:
            orders = Order.objects.filter(user=self.request.user).filter(
                provider=provider).order_by("-id")
        else:
            orders = Order.objects.filter(
                user=self.request.user).order_by("-id")
        context["orders"] = orders
        context["title"] = "history"
        return context


def check_multiplicity(cart: list, i_provider=""):
    _cart = sorted(cart, key=lambda item: item.get("provider"), reverse=False)
    if i_provider != "":
        provider = i_provider
        for item in _cart.copy():
            if item.get("provider") != provider:
                index = _cart.index(item)
                _cart.pop(index)
    for item in _cart:
        try:
            if int(item["quantity"]) % int(item["multiplicity"]) != 0:
                return False
        except ValueError:
            pass
    return True


class CreateOrder(View):
    model = Order

    def post(self, request: HttpRequest, i_provider=0):
        self.request = request
        cart = request.session.get("cart")
        if not cart:
            return redirect("/cart?error_msg=Добавьте хотя бы один товар")
        self.cart = cart
        ignore = self.request.POST.get("ignore")
        if not ignore:
            if str(i_provider) == "0":
                if not check_multiplicity(cart):
                    return redirect("/cart/?multiplicity=1")
            else:
                if not check_multiplicity(cart, i_provider):
                    return redirect("cart")
        if str(i_provider) == "0":
            status = check_min_sum(cart, request.session["min_sums"])
        else:
            status = check_min_sum(cart, request.session[
                                   "min_sums"], i_provider)
        if not status[1]:
            error_url = "/cart?error=" + status[0]
            return redirect(error_url)
        if str(i_provider) == "0":
            self.buy_all()
            return redirect("/clear/?redir=/")
        else:
            self.buy_provider(i_provider)
            return redirect("/clear/?provider=" + i_provider)

    def buy_all(self):
        order = Order()
        order.user = self.request.user
        order.save()
        cart = sorted(self.cart, key=lambda item: item.get("provider"),
                      reverse=False)
        provider = cart[0].get("provider")
        _provider = Provider.objects.filter(name=provider)[0]
        order.provider = _provider
        order.save()
        _sum = 0
        for item in cart:
            product: Product = Product.objects.get(id=int(item["id"]))
            product.quantity = product.quantity - int(item["quantity"])
            product.save()
            if str(provider) != str(product.provider) and order.summary > 0:
                order.summary = _sum
                order.save()
                _sum = 0
                product_set = order.get_product_set()
                create_csv(product_set, self.request.user, order)
                order = Order()
                order.user = self.request.user
                provider = product.provider
                order.provider = provider
                order.save()
            order_p = OrderProduct()
            order_p.name = product.name
            order_p.barcode = product.barcode
            order_p.provider = product.provider
            order_p.quantity = item['quantity']
            order_p.cost = product.cost
            order_p.order = order
            _sum += order_p.cost * int(order_p.quantity)
            order_p.save()
        order.summary = _sum
        order.save()
        product_set = order.get_product_set()
        try:
            create_csv(product_set, self.request.user, order)
            return redirect('/admin/main/order')
        except AttributeError:
            return HttpResponse("Возникла ошибка при отправке вашего заказа <b>код ошибки</b>: 702, \
                пожалуйста свяжитесь с администратором для устранения этой проблемы")

    def buy_provider(self, provider):
        order = Order()
        order.user = self.request.user
        order.save()
        _cart = self.cart.copy()
        index_array = list()
        for item in _cart.copy():
            if item.get("provider") != provider:
                index = _cart.index(item)
                index_array.append(index)
        index_array.reverse()
        for index in index_array:
            _cart.pop(index)
        _sum = 0
        provider: Provider
        for item in _cart:
            product = Product.objects.get(id=int(item["id"]))
            product.quantity = product.quantity - int(item["quantity"])
            product.save()
            provider = product.provider
            order_p = OrderProduct()
            order_p.name = product.name
            order_p.barcode = product.barcode
            order_p.provider = product.provider
            order_p.quantity = item['quantity']
            order_p.cost = product.cost
            _sum += int(item['quantity']) * product.cost
            order_p.order = order
            order_p.save()
        order.provider = provider
        order.summary = _sum
        order.save()
        product_set = order.get_product_set().order_by("provider")
        try:
            create_csv(product_set, self.request.user, order)
            return redirect('/admin/main/order')
        except AttributeError:
            return HttpResponse("Возникла ошибка при отправке вашего заказа <b>код ошибки</b>: 702, \
                пожалуйста свяжитесь с администратором для устранения этой проблемы")


def check_min_sum(cart: List[Dict[str, str]], min_sums: Dict[str, str], i_provider: str="") -> Tuple[str, bool]:
    _cart = sorted(cart, key=lambda item: item.get("provider"), reverse=False)
    _sum = 0
    provider = _cart[0].get("provider")
    if i_provider != "":
        provider = i_provider
        for item in _cart.copy():
            if item.get("provider") != provider:
                index = _cart.index(item)
                _cart.pop(index)
    for item in _cart:
        if item.get("provider") != provider:
            if min_sums.get(provider) is None:
                continue
            if _sum < int(min_sums.get(provider)):
                return provider, False
            _sum = 0
            provider = item.get("provider")
        _sum += float(item.get("sum"))

    if min_sums.get(provider) is None:
        return "", True
    if _sum < int(min_sums.get(provider)):
        return provider, False
    return "", True


def send_message(user: User, provider: Provider, subject: str, message_text: str, path_to_file: str):
    service = createService(user)
    body = create_message(user, provider, subject, message_text, path_to_file)
    service.users().messages().send(userId='me', body=body).execute()


def create_message(user: User, provider: Provider, subject: str, message_text: str, path_to_file: str) -> dict:
    message = MIMEMultipart()
    message['to'] = provider.email_listener
    message['from'] = user.email
    message['subject'] = subject
    msg = MIMEText(message_text)
    message.attach(msg)
    path, filename = os.path.split(path_to_file)
    filename = path + "/" + filename.split(".")[0] + "_converted.tsv"
    command = "iconv -f utf-8 -t windows-1251 {0} > {1}".format(
        path_to_file, filename)
    os.system(command)
    content_type, encoding = mimetypes.guess_type(filename)
    main_type, sub_type = content_type.split('/', 1)
    fp = open(filename, 'rb')
    msg = MIMEBase(main_type, sub_type)
    msg.set_payload(fp.read())
    fp.close()
    filename = os.path.basename(filename)
    msg.add_header('Content-Disposition', 'attachment', filename=filename)
    message.attach(msg)
    raw = base64.urlsafe_b64encode(message.as_bytes())
    raw = raw.decode()
    return {'raw': raw}


def create_csv(product_set, user: User, order: Order):
    provider = product_set[0].provider
    csv_text = '{0}\t\n{1}\t\n{2}\t\n{3}\t\n{4}\t\nШтрихкод\tНаименование\tЦена\tКоличество\t\n'.format(order.id,
                                                                                                        provider.id,
                                                                                                        user.organization,
                                                                                                        user.physical_adress,
                                                                                                        order.get_format_date())
    for product in product_set:
        if product.provider != provider:
            path_to_file = os.path.expanduser(
                "~/sending/") + "{0}_{1}_{2}.csv".format(str(user.id), provider, "")
            with open(path_to_file, "w") as f:
                f.write(csv_text)
            send_message(user, provider, "заказ", "", path_to_file)
            provider = product.provider
            csv_text = '"{0}\t\n{1}\t\n{2}\t\n{3}\t\n{4}\t\nШтрихкод\tНаименование\tЦена\tКоличество\t\n'.format(order.id,
                                                                                                        provider.id,
                                                                                                        user.organization,
                                                                                                        user.physical_adress,
                                                                                                        order.get_format_date())
        csv_template = '{0}\t{1}\t{2}\t{3}\t\n'.format(
            product.barcode, product.name, product.cost, product.quantity)
        csv_text += csv_template
    path_to_file = os.path.expanduser(
        "~/sending/") + "{0}_{1}.tsv".format(str(user.id), provider)
    provider = product.provider
    with open(path_to_file, "w") as f:
        f.write(csv_text)
    send_message(user, provider, "заказ", "", path_to_file)


def get_credentials(user):
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    file_name = "gmail-python-quickstart-" + str(user.id) + ".json"
    credential_path = os.path.join(credential_dir,
                                   file_name)
    store = Storage(credential_path)
    credentials = store.get()
    return credentials


def createService(user):
    credentials = get_credentials(user)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    return service


def send_again(request: HttpRequest, order_id: str):
    user = Order.objects.get(id=int(order_id)).user
    product_set = Order.objects.get(id=int(order_id)).get_product_set()
    try:
        create_csv(product_set, user)
        return redirect('/admin/main/order')
    except AttributeError:
        return HttpResponse("Возникла ошибка при отправке вашего заказа <b>код ошибки</b>: 702,\
            пожалуйста свяжитесь с администратором для устранения этой проблемы")
