from django.shortcuts import redirect
from django.views.generic import ListView, FormView
from django.contrib.auth import login, logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from main.models import Product, Provider
from main.forms import LoginForm
from reference.models import Product as ReferenceProduct


class CommonView(ListView):
    model = Product
    template_name = "index.html"
    paginate_by = 50

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.results = []
        self.context = {}

    def get_provider_set(self):
        return list(Provider.objects.filter(work_with=self.request.user))

    def get_context_data(self, **kwargs) -> dict:
        context: dict = super(CommonView, self).get_context_data(**kwargs)
        context.pop("object_list")
        try:
            context.pop("product_list")
        except KeyError as _:
            pass
        self.check_variables()
        context["show"] = self.request.session.get("show")
        context["min_date"] = self.request.session.get("min_date")
        context["cart"] = self.request.session.get("cart")
        context["simple_cart"] = self.request.session.get("simple_cart")
        context["min_sums"] = self.request.session.get("min_sums")
        context["user"] = self.request.user
        self.context = context
        return context

    def check_variables(self):
        self.check_show()
        self.check_min_date()
        self.check_cart()
        self.check_min_sums()
        self.check_simple_cart()

    def check_show(self):
        if not self.request.session.get("show"):
            self.request.session["show"] = "true"

    def check_min_date(self):
        if not self.request.session.get("min_date"):
            self.request.session["min_date"] = 1

    def check_cart(self):
        if not self.request.session.get("cart"):
            cart = []
            self.request.session["cart"] = cart

    def check_min_sums(self):
        if not self.request.session.get("min_sums"):
            min_sums = {}
            self.request.session["min_sums"] = min_sums

    def check_simple_cart(self):
        if not self.request.session.get("simple_cart"):
            simple_cart = []
            self.request.session["simple_cart"] = simple_cart

    def get_results(self, keywords):
        ref_products = ReferenceProduct.objects.all()
        for keyword in keywords:
            ref_products = ref_products.filter(name__contains=keyword)
        results = list()
        providers = self.get_provider_set()
        products = Product.objects.all().filter(user=self.request.user).exclude(quantity__lte=0)
        for ref_product in ref_products:
            barcode = ref_product.barcode
            products_clone = [product for product in products if product.barcode == barcode]
            if self.request.GET.get("provider"):
                provider = Provider.objects.get(id=int(self.request.GET.get("provider")))
                _products = []
                for product in products_clone:
                    if product.provider == provider \
                            and product not in _products \
                            and product not in results:
                        _products.append(product)
                results.extend(_products)
            else:
                for provider in providers:
                    _products = []
                    for product in products_clone:
                        if product.provider == provider \
                                and product not in _products \
                                and product not in results:
                            _products.append(product)
                    results.extend(_products)
        self.results = results
        self.sort_by_cost()

    def sort_by_name(self):
        self.results = sorted(self.results, key=lambda item: item.name)
        if self.request.GET.get("name") == "2":
            self.results.reverse()

    def sort_by_cost(self):
        self.results = sorted(self.results, key=lambda item: item.cost)
        if self.request.GET.get("cost") == "2":
            self.results.reverse()

    def build_paginator(self):
        if self.request.GET.get("name"):
            self.sort_by_name()
        else:
            self.sort_by_cost()
        paginator = Paginator(self.results, self.paginate_by)
        context = {"paginator": paginator, "min_date": self.request.session["min_date"],
                   "show": self.request.session["show"]}
        try:
            self.results = paginator.page(self.request.GET.get("page"))
        except PageNotAnInteger:
            self.results = paginator.page(1)
        except EmptyPage:
            self.results = paginator.page(paginator.num_pages)
        context["page_obj"] = self.results
        context["results"] = zip(
            self.results,
            map(lambda product: product.get_status(
                context["min_date"]), self.results),
            range(1, len(self.results) + 1),
        )
        self.context.update(context)


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = "/catalog"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("/catalog")
        else:
            return super(LoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.login()
        if user is not None:
            login(request=self.request, user=user)
            return super(LoginView, self).form_valid(form)
        else:
            form.add_error(None, error={"email": "Логин и/или пароль введён неверно"})
            return super(LoginView, self).form_invalid(form)

    def form_invalid(self, form):
        return super(LoginView, self).form_invalid(form)


def logout_view(request):
    logout(request)
    return redirect("/")
