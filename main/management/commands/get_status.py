from __future__ import print_function

import httplib2
import os

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from main.models import Provider

from apiclient import discovery
from oauth2client.file import Storage

SCOPES = ['https://www.googleapis.com/auth/gmail.readonly',
          'https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/gmail.send',
          'https://mail.google.com/']

CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'


def get_credentials(user: User):
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    file_name = "gmail-python-quickstart-" + str(user.id) + ".json"
    credential_path = os.path.join(credential_dir,
                                   file_name)
    if not os.path.isfile(credential_path):
        raise FileNotFoundError
    store = Storage(credential_path)
    credentials = store.get()
    return credentials


def create_service(user: User):
    credentials = get_credentials(user)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    return service


def read_msgs(user) -> int:
    # This function download files and returns files_info
    service = create_service(user)
    count = 0
    for provider in Provider.objects.filter(user=user):
        query = "from:{0} is:unread".format(provider.email_sender)
        results = service.users().messages().list(userId='me',
                                                  q=query).execute()
        if results == {'resultSizeEstimate': 0}:
            continue
        else:
            count += 1
    return count


class Command(BaseCommand):
    help = 'Проверить наличие новых прайсов'

    def handle(self, *args, **options):
        for user in User.objects.all():
            if user.is_superuser:
                continue
            try:
                count = read_msgs(user)
                if count != 0:
                    user.is_notified = False
                    user.new_notifications = count
                    user.save()
                else:
                    user.is_notified = True
                    user.new_notifications = 0
                    user.save()
            except FileNotFoundError:
                print("Не найден файл gmail api {0}".format(user))
                continue
