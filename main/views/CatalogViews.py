from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import redirect

from main.models import Product
from .CommonViews import CommonView

import re


class CatalogView(CommonView):
    template_name = "search.html"

    def get_context_data(self, **kwargs):
        self.context: dict = super().get_context_data(**kwargs)
        results = list()
        products = Product.objects.all().exclude(barcode='').filter(quantity__gt=0, user=self.request.user)
        for provider in self.get_provider_set():
            results.extend(products.filter(provider=provider))
        self.results = results
        self.build_paginator()
        self.context["title"] = "catalog"
        return self.context


class SearchView(CommonView):
    template_name = "search.html"

    def get(self, request, *args, **kwargs):
        keywords = request.GET.get("q")
        if keywords == "" and request.GET.get("provider") is None:
            return redirect("/catalog")
        return super(SearchView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        self.context: dict = super(SearchView, self).get_context_data(**kwargs)
        keywords = self.request.GET.get("q")
        q = keywords
        keywords = keywords.upper()
        keywords = re.findall("\w+", keywords)
        self.get_results(keywords)
        self.build_paginator()
        self.context["query"] = q
        self.context["title"] = "catalog"
        return self.context


def get_notify_status(request: HttpRequest):
    user: User = request.user
    data = {"status": int(user.is_notified), "notifications": user.new_notifications}
    return JsonResponse(data)


def set_notify_status(request: HttpRequest):
    user: User = request.user
    user.is_notified = True
    user.save()
    return HttpResponse("1")
