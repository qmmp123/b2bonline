from typing import List
from collections import Counter

from django.contrib import admin

from reference.models import Product, Category, ItemGroup


@admin.register(ItemGroup)
class ItemGroupAdmin(admin.ModelAdmin):
    pass


class OverlapFilter(admin.SimpleListFilter):
    def queryset(self, request, queryset):
        barcodes: List[str] = [product.barcode for product in Product.objects.all()]
        c = Counter(barcodes)
        overlaps = [[k, ]*v for k, v in c.items()]
        ids = []
        for barcode in overlaps:
            if len(barcode) > 1:
                for product in Product.objects.filter(barcode=barcode[0]):
                    ids.append(product.id)
        if self.value() == "1":
            return Product.objects.filter(id__in=ids).order_by('barcode')
        else:
            return queryset.all()

    def lookups(self, request, model_admin):
        return (
            ('0', 'Ничего не делать'),
            ("1", 'Показать'),
        )

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': changelist.get_query_string({self.parameter_name: lookup}, []),
                'display': title,
            }

    title = "Совпадения"
    parameter_name = "overlap"


# Register your models here.
@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    """
        Admin View for Product
    """
    list_filter = ('vendor', 'category', 'item_group', OverlapFilter)
    search_fields = ('name', 'barcode')
    list_display = (
        'name', 'barcode', 'vendor', 'packing', 'dosage', 'release_form', 'item_group', 'get_vital', 'get_category')


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass

    # TODO: В ЖВ только галочку там где "Да"
