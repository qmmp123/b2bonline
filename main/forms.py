from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
# from .models import Product, Price, Provider


class LoginForm(forms.Form):
    email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={
        'class': 'form-control'
    }), error_messages={"invalid": "email, который вы ввели неверен"},
                             required=True)
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        "class": "form-control"
    }), label="Пароль", required=True)

    class Meta:
        model = User

    def login(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        user = authenticate(username=email, password=password)
        return user
