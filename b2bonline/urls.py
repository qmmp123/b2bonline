"""b2bonline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from main.views.CartViews import *
from main.views.CatalogViews import *
from main.views.SettingsViews import *
from main.views.OrderViews import *
from main.views.CommonViews import *

from reference.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/reference/import_reference/', login_required(ImportProducts.as_view())),
    
    url(r'^$', LoginView.as_view()),
    url(r'^logout/', login_required(logout_view)),
    
    url(r'^search/', login_required(SearchView.as_view())),
    url(r'^catalog/', login_required(CatalogView.as_view())),
    url(r'^get_notify_status', login_required(get_notify_status)),
    url(r'^set_notify_status', login_required(set_notify_status)),
    url(r'^update_price', login_required(update_price)),

    url(r'^settings/', login_required(SettingsView.as_view())),
    url(r'^attach_to_user/', attach_to_user),
    url(r'^detach_from_user/', detach_from_user),
    url(r'^set_min_date/', date_filters),
    url(r'^change_min_order/', change_min_order),

    url(r'^add_to_cart/', login_required(add_to_cart)),
    url(r'^delete_from_cart/', login_required(delete_from_cart)),
    url(r'^cart/', login_required(CartView.as_view())),
    url(r'^clear/', login_required(clear)),
    url(r'^update_table/', login_required(get_provider_cart)),
    url(r'^update_quantity', login_required(update_quantity)),

    url(r'^history/', login_required(HistoryView.as_view())),
    url(r'^buy/(?P<i_provider>[\w\-]+)', login_required(CreateOrder.as_view())),

    url(r'^send_again/(?P<order_id>\w+)/', login_required(send_again)),

]

