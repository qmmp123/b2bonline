from __future__ import print_function

import datetime
from typing import List, Dict

import xlrd

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from main.models import Product, Provider

import httplib2
import os
import base64

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage


SCOPES = ['https://www.googleapis.com/auth/gmail.readonly',
          'https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/gmail.send',
          'https://mail.google.com/']

CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Quickstart'


def get_credentials(user: User):
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    file_name = "gmail-python-quickstart-" + str(user.id) + ".json"
    credential_path = os.path.join(credential_dir,
                                   file_name)
    if not os.path.isfile(credential_path):
        raise SomeException

    store = Storage(credential_path)
    credentials = store.get()
    # if not credentials or credentials.invalid:
    #     flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
    #     flow.user_agent = APPLICATION_NAME
    #     credentials = tools.run(flow, store)
    #     print('Storing credentials to ' + credential_path)
    return credentials


def createService(user: User):
    credentials = get_credentials(user)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)
    return service

def getAttachment(service, result, msg) -> str:
    for part in msg['payload']['parts']:
            if part['filename']:
                if 'data' in part['body']:
                    data=part['body']['data']
                else:
                    att_id=part['body']['attachmentId']
                    att=service.users().messages().attachments().get(userId='me', messageId=result["id"],id=att_id).execute()
                    data=att['data']
                file_data = base64.urlsafe_b64decode(data.encode('UTF-8'))
                path = os.path.expanduser("~/prices/") + part['filename']
                with open(path, 'wb') as f:
                    f.write(file_data)
                return path


def getEmailSender(query: str)-> str:
    email = query.split(" ")[0].split(":")[1]
    return email

def readMsgs(user, msgs) -> List[Dict[str, str]]:
    # This function download files and returns files_info
    service = createService(user)
    queries = []
    for provider in Provider.objects.filter(user=user):
        query = "from:{0} is:unread".format(provider.email_sender)
        queries.append(query)
    files_info = []
    for query in queries:
        file_info = {"provider": "", "file_path": ""}
        results = service.users().messages().list(userId='me',
            q=query).execute()
        if results == {'resultSizeEstimate': 0}:
            continue
        for result in results["messages"]:
            msg = service.users().messages().get(userId='me', id=result["id"]).execute()
            email = getEmailSender(query)
            file_info["provider"] = Provider.objects.filter(email_sender=email)[0]
            file_info["file_path"] = getAttachment(service, result, msg)
            files_info.append(file_info)
            service.users().messages().modify(userId='me', id=result["id"],
                body={'removeLabelIds': ["UNREAD"]}).execute()
    return files_info


def getMsgs(user: User) -> List[Dict[str, str]]:
    service = createService(user)
    msgs = service.users().messages().list(userId='me').execute()
    files_info = readMsgs(user, msgs["messages"])
    return files_info


class SomeException(Exception):
    pass


class Command(BaseCommand):
    help = 'Загрузить новые прайсы'

    def handle(self, *args, **options):
        for user in User.objects.all():
            try:
                file_paths = getMsgs(user)
                self.create_products(user, file_paths)
            except SomeException:
                continue

    def create_products(self, user: User, files_info: List[Dict[str, str]]):
        for file_info in files_info:
            Product.objects.all().filter(provider=file_info["provider"]).filter(user=user).delete()
            rb = xlrd.open_workbook(file_info["file_path"])
            sheet = rb.sheet_by_index(0)
            for rownum in range(sheet.nrows):
                if rownum == 0:
                    continue
                row = sheet.row_values(rownum)
                x = 0
                product = Product()
                product.provider = file_info["provider"]
                product.user = user
                err = False
                for c_el in row:
                    if x == 0:
                        try:
                            product.barcode = str(c_el).split(".0")[0]
                        except ValueError:
                            err = True
                    elif x == 1:
                        try:
                            product.name = str(c_el).upper()
                        except ValueError:
                            err = True
                    elif x == 2:
                        try:
                            product.cost = c_el
                        except ValueError:
                            err = True
                    elif x == 3:
                        try:
                            product.shelf_life = datetime.datetime.strptime(c_el, "%d.%m.%Y")
                        except ValueError:
                            product.shelf_life = None
                    elif x == 4:
                        try:
                            product.quantity = c_el
                        except ValueError:
                            err = True
                    x += 1
                if not err:
                    product.save()
                else:
                    continue
            os.system('rm ' + file_info["file_path"])
