from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.management import call_command
from django.utils.six import StringIO

from reference.models import Product as ReferenceProduct
from main.models import Product, Provider

import os
# Create your tests here.


# Need tests for cart(add, delete, clear), order(send, create, history), search(results)

class CommandTest(TestCase):

    def test_price_folder(self):
        dir = os.listdir(os.path.expanduser("~"))
        self.assertIn("prices", dir, msg="")

    def test_command_successful(self):
        out = StringIO()
        call_command('import_products', stdout=out)


class OrderTests(TestCase):

    def test_sending_folder(self):
        dir = os.listdir(os.path.expanduser("~"))
        self.assertIn("sending", dir, msg="NO FOLDER FOR CSV FILES")
        self.assertIn(".credentials", dir, msg="NO FOLDER FOR GMAIL JSON")

    def test_gmail_json(self):
        dir = os.listdir(os.path.expanduser("~/.credentials"))
        user_ids = list()
        for file in dir:
            file: str
            name, _ = file.split(".")
            *name, user_id = name.split("-")
            user_ids.append(user_id)
        users = list(User.objects.all())
        count = len(users) - 1
        for user in users:
            if str(user.id) in user_ids:
                count -= 1
        self.assertEqual(0, count, msg="Не для всех заведён json файл")



class CatalogViewTests(TestCase):
    """
    Тестирует пагинацию, куки
    """
    def setUp(self):
        self.user = User.objects.get(id=2)
        client = Client()
        client.login(username='b2bonline.pro@gmail.com', password="retro123")
        self.client = client

    def test_len_results_per_page(self):
        length = 0
        for provider in Provider.objects.filter(work_with=self.user):
            length = Product.objects.filter(user=self.user, provider=provider)\
            .exclude(quantity__lte=0, barcode='').count()
        response = self.client.get("/catalog/")
        paginator = response.context["paginator"]
        for i in paginator.page_range:
            page = paginator.page(i)
            length -= len(page.object_list)
        self.assertEqual(length, 0)

    def test_cookie_statements(self):
        response = self.client.get("/catalog/")
        self.assertEqual("true", response.context["show"])
        self.assertEqual(1, response.context["min_date"])
        self.assertListEqual([], response.context["cart"])
        self.assertListEqual([], response.context["simple_cart"])
        self.assertDictEqual({}, response.context["min_sums"])


class LoginTests(TestCase):

    def setUp(self):
        client = Client()
        client.login(username='b2bonline.pro@gmail.com', password="retro123")
        self.client = client

    def test_login(self):
        response = self.client.get("/catalog", follow=True)
        self.assertEqual(response.status_code, 200)

    def test_login_redirect(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 302)
