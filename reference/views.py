import decimal

from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import FormView

from .forms import ImportForm
from .models import Product as ProductReference, ItemGroup, Category

import xlrd
import datetime

import os


class ImportProducts(FormView):
    template_name = "admin/reference/product/import_form.html"
    form_class = ImportForm
    success_url = "/admin/reference/product/"

    def get_context_data(self, **kwargs):
        context = super(ImportProducts, self).get_context_data(**kwargs)
        context['title'] = "Выберите файл"
        return context

    def form_invalid(self, form):
        return super().form_invalid(form)

    def form_valid(self, form):
        self.path = "{0}{2}{1}".format(os.path.expanduser("~/prices/"), form.files['file'],
                                       datetime.datetime.now().second)
        with open(self.path, 'wb+') as destination:
            for chunk in form.files['file'].chunks():
                destination.write(chunk)
        self.create_reference()
        os.system('rm ' + self.path)
        return super().form_valid(form)

    def create_reference(self):
        product_attrs = {
            0: "name",
            1: "barcode",
            2: "vendor",
            3: "packing",
            4: "dosage",
            5: "release_form",
            6: "vital",
            7: "item_group",
            8: "category"
        }
        rb = xlrd.open_workbook(self.path)
        sheet = rb.sheet_by_index(0)
        for rownum in range(sheet.nrows):
            if rownum == 0:
                continue
            row = sheet.row_values(rownum)
            x = 0
            product = ProductReference()
            for c_el in row:
                value = c_el
                key = product_attrs[x]
                if key == "category":
                    try:
                        product.save()
                        category = Category.objects.get(name=value.upper())
                        product.category.add(category)
                    except ObjectDoesNotExist:
                        category = Category(name=value)
                        category.save()
                        product.category.add(category)
                elif key == "item_group":
                    try:
                        item_group = ItemGroup.objects.get(name=value.upper())
                    except ObjectDoesNotExist:
                        item_group = ItemGroup(name=value.upper())
                        item_group.save()
                    finally:
                        product.item_group = item_group
                elif key == "vital":
                    value = bool(int(value))
                    setattr(product, key, value)
                elif key == "packing":
                    try:
                        value = float(format(float(value), ".6f"))
                    except ValueError:
                        value = float(format(float(".".join(str(value).split(","))), ".6f"))
                    setattr(product, key, value)
                else:
                    setattr(product, key, value)
                x += 1
