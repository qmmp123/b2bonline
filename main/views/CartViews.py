from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseBadRequest, HttpRequest

from main.models import Product

from .CommonViews import CommonView
from . import gmail_api_funcs as gaf

from typing import List, Dict, Union
import json


class CartView(CommonView):
    template_name = "cart.html"

    def get_context_data(self, **kwargs)-> dict:
        context = super(CartView, self).get_context_data(**kwargs)
        providers = []
        _providers = []
        for item in context["cart"]:
            provider_info = {"provider": "", "quantity": 0}
            if item["provider"] not in _providers:
                provider_info["provider"] = item["provider"]
                provider_info["quantity"] += 1
                providers.append(provider_info)
                _providers.append(item["provider"])
            else:
                for p_io in providers:
                    if p_io["provider"] == item["provider"]:
                        p_io["quantity"] += 1
        context["providers"] = providers
        context["provider_error"] = self.request.GET.get("error")
        context["error_msg"] = self.request.GET.get("error_msg")
        context["multiplicity_warning"] = self.request.GET.get("multiplicity")
        context["title"] = "cart"
        return context


def get_provider_cart(request: HttpRequest):
    provider = request.GET.get("provider")
    context = {}
    if provider != "":
        cart = request.session.get("cart")
        for item in cart.copy():
            if item["provider"] != provider:
                cart.remove(item)
        context["cart"] = cart
        return HttpResponse(json.dumps(context), content_type="application/json")
    else:
        cart = request.session.get("cart")
        context["cart"] = cart
        return HttpResponse(json.dumps(context), content_type="application/json")


def add_to_cart(request: HttpRequest):
    cart = request.session.get("cart")
    id_product = int(request.GET.get("id"))
    product = Product.objects.get(id=id_product)
    quantity = request.GET.get("quantity")
    a = int(quantity) * float(product.cost)
    item = {"id": id_product, "quantity": int(quantity),
            "name": product.name, "price": product.cost, "sum": "%.2f" % a,
            "provider": str(product.provider), "multiplicity": str(product.multiplicity)
            }
    item_in_cart = check_item(cart, item)
    if not item_in_cart:
        cart.append(item)
        simple_cart: list = request.session.get("simple_cart")
        simple_cart.append(id_product)
        request.session["simple_cart"] = simple_cart
    else:
        cart.remove(item_in_cart)
        item_in_cart["quantity"] += int(item["quantity"])
        item_in_cart["sum"] = item_in_cart["quantity"] * product.cost
        item_in_cart = check_item_quantity(item_in_cart)
        cart.append(item_in_cart)
    request.session["cart"] = cart
    return HttpResponse("Success")


def delete_from_cart(request: HttpRequest):
    cart = request.session.get("cart")
    for item_cart in cart:
        if str(item_cart["id"]) == str(request.GET.get("id")):
            cart.remove(item_cart)
            break
    request.session["cart"] = cart
    simple_cart: List[str] = request.session.get("simple_cart")
    index = simple_cart.index(int(request.GET.get("id")))
    simple_cart.pop(index)
    request.session["simple_cart"] = simple_cart
    return redirect("/cart")


def clear(request: HttpRequest):
    if request.GET.get("provider"):
        simple_cart = request.session.get("simple_cart")
        provider = request.GET.get("provider")
        cart: List[Dict[str, str]] = request.session.get("cart")
        index_array = []
        for item in cart:
            if item.get("provider") == provider:
                simple_cart.pop(simple_cart.index(int(item.get("id"))))
                index = cart.index(item)
                index_array.append(index)
        c = 0
        for index in index_array:
            cart.pop(index-c)
            c += 1
        request.session["cart"] = cart
        request.session["simple_cart"] = simple_cart
        return redirect("/cart/")
    cart: List[Dict[str, str]] = []
    simple_cart = []
    request.session["cart"] = cart
    request.session["simple_cart"] = simple_cart
    if request.GET.get("redir"):
        return redirect(request.GET.get("redir"))
    return redirect('/cart/')


def check_item_quantity(item: Dict[str, str]) -> Union[Dict[str, str], bool]:
    product = Product.objects.get(id=int(item["id"]))
    if product.quantity < int(item["quantity"]):
        item["quantity"] = product.quantity
        item["sum"] = item["quantity"] * product.cost
        return item
    return item


def check_item(cart: List[Dict[str, str]], item: Dict[str, str]) -> Union[Dict[str, str], bool]:
    for cart_item in cart:
        if item["id"] == cart_item["id"]:
            return cart_item
    return False


def update_quantity(request: HttpRequest):
    cart = request.session.get("cart")
    for item_in_cart in cart:
        if item_in_cart.get("id") == int(request.GET.get("id")):
            product = Product.objects.get(id=int(request.GET.get("id")))
            if product.quantity >= int(request.GET.get("quantity")):
                item = item_in_cart
                cart.remove(item_in_cart)
                item["quantity"] = request.GET.get("quantity")
                a = float(item["quantity"]) * float(product.cost)
                item["sum"] = "%.2f" % a
                print(item["sum"])
                cart.append(item)
                request.session["cart"] = cart
                return HttpResponse("succesfull")
            else:
                return HttpResponseBadRequest(product.quantity)


def update_price(request: HttpRequest):
    user: User = request.user
    file_paths = gaf.get_msgs(user)
    for file in file_paths:
        simple_cart = request.session.get("simple_cart")
        provider = file["provider"]
        cart: List[Dict[str, str]] = request.session.get("cart")
        index_array = []
        for item in cart:
            if item.get("provider") == provider.__str__():
                simple_cart.pop(simple_cart.index(int(item.get("id"))))
                index = cart.index(item)
                index_array.append(index)
        c = 0
        for index in index_array:
            cart.pop(index - c)
            c += 1
        request.session["cart"] = cart
        request.session["simple_cart"] = simple_cart
    gaf.create_products(user, file_paths)
    user.new_notifications = 0
    user.is_notified = True
    user.save()
    return redirect("/")
