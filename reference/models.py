from django.db import models
from django.contrib.admin.templatetags.admin_list import _boolean_icon

# Create your models here.


class Category(models.Model):
    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    name = models.CharField(max_length=200, default="", verbose_name="Название категории")

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.name = self.name.upper()
        super().save()


class ItemGroup(models.Model):
    class Meta:
        verbose_name = "Номенкулатурная группа"
        verbose_name_plural = "Номенкулатурные группы"

    name = models.CharField(max_length=200, default="", verbose_name="Название номенкулатурной группы")

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.name = self.name.upper()
        super().save()


class Product(models.Model):
    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    name = models.CharField(max_length=200, default="", verbose_name="Название продукта")
    barcode = models.CharField(max_length=200, verbose_name="Штрихкод")
    vendor = models.CharField(max_length=200, verbose_name="Производитель", blank=True, null=True)
    packing = models.CharField(max_length=200, verbose_name="Фасовка", blank=True, null=True)
    dosage = models.CharField(max_length=200, verbose_name='Дозировка', blank=True, null=True)
    release_form = models.CharField(max_length=200, verbose_name='Форма выпуска', blank=True, null=True)
    vital = models.BooleanField(default=False, verbose_name="ЖВ", blank=True)
    original = models.NullBooleanField(default=False, verbose_name='Оригинал', blank=True, null=True)

    item_group = models.ForeignKey(ItemGroup, verbose_name="Номенкулатурная группа", blank=True, null=True)
    category = models.ManyToManyField(Category, verbose_name="Категория", related_name="Категория")

    def __str__(self):
        return self.name

    def get_category(self):
        return ",".join([p.name for p in self.category.all()])

    def get_vital(self):
        if self.vital:
            return _boolean_icon(self.vital)
        else:
            return ""

    get_vital.short_description = 'ЖВ'
    get_vital.allow_tags = True
    get_vital.admin_order_field = 'vital'
    get_category.short_description = 'Категория'
