from django.contrib import admin

from main.models import Product, Provider, Order
from django.contrib.auth.models import User
# Register your models here.


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    exclude = ["work_with"]


#def move_to_other_user(modesladmin, request, queryset):
    #user = User.objects.get(id=2)
    #queryset.update(user=user)
#move_to_other_user.short_description = "Перенести всё к b2bonline"


#@admin.register(Product)
#class ProductAdmin(admin.ModelAdmin):
    #search_fields = ['name', 'barcode']
    #list_filter = ['provider__name', 'user__organization']
    #list_display = ('name', 'quantity', 'cost')
    #actions = [move_to_other_user]
    #exclude = ["user"]


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    change_list_template = 'admin/main/order/change_list.html'
    search_fields = ["id", "provider__name"]
#    list_filter = ('provider__name', 'user__organization')
    list_display = ('id', 'user', "provider", "summary", "date")

    def change_view(self, request, object_id, **kwargs):
        extra_context = {"is_order": True}
        return super(OrderAdmin, self).change_view(request, object_id, extra_context=extra_context, **kwargs)
