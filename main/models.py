from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta, datetime


# Create your models here.
def format_date(string):
    if len(str(string)) == 1:
        string = "0" + str(string)
    return string


class Days(models.Model):
    day = models.CharField(default="", max_length=200, verbose_name="День")

    def __str__(self):
        return self.day


class Provider(models.Model):
    class Meta:
        verbose_name = 'Поставщик'
        verbose_name_plural = "Поставщики"

    name = models.CharField(max_length=200, default="", verbose_name="Имя поставщика")
    INN = models.CharField(max_length=200, default="", verbose_name="ИНН")
    pay_type = models.BooleanField(
        default=True, verbose_name="Тип оплаты(если галочка стоит значит по предоплате)")
    deliver_time = models.IntegerField(
        default=1, blank=True, null=True, verbose_name="Время доставки")
    min_order = models.FloatField(
        default=0.0, verbose_name="Минимальная сумма заказа")
    fast_delivering = models.BooleanField(
        default=False, verbose_name="Быстрая доставка")
    contact_name = models.CharField(
        default="", max_length=200, verbose_name="Контактное лицо")
    email_sender = models.EmailField(
        verbose_name="Почта с которой приходят прайсы")
    email_listener = models.EmailField(
        verbose_name="Почта на которую надо отправлять заказы")
    phone_number = models.CharField(
        verbose_name="Номер телефона", max_length=17, default="+7 999 321 23 23")

    graph_days = models.ManyToManyField(
        Days, max_length=200, blank=True, verbose_name="График работы")
    user = models.ManyToManyField(
        User, blank=True, verbose_name="Доступен ли он пользователю", related_name="user")
    work_with = models.ManyToManyField(
        User, blank=True, verbose_name="Работает ли с ним пользователь", related_name="work_with")

    def __str__(self):
        return self.name


class Order(models.Model):
    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = "Заказы"

    user = models.ForeignKey(User, verbose_name="Покупатель", blank=True)
    date = models.DateTimeField(
        default=datetime.now, blank=True, verbose_name="Дата заказа")
    summary = models.FloatField(default=0.0, verbose_name="Сумма заказа")
    provider = models.ForeignKey(
        Provider, verbose_name='Поставщик', blank=True, null=True)

    def get_summary(self):
        ops = self.get_product_set()
        summary = 0.0
        for product in ops:
            summary += product.cost * product.quantity
        return "%.2f" % summary

    def get_product_set(self):
        return OrderProduct.objects.filter(order=self)

    def get_format_date(self):
        date = "{0}.{1}.{2}  {3}:{4}".format(format_date(self.date.day),
                                             format_date(self.date.month),
                                             self.date.year,
                                             format_date(self.date.hour),
                                             format_date(self.date.minute)
                                             )
        return date

    def __str__(self):
        return "Заказ №{0}".format(self.id)


class Product(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = "Товары"
        ordering = ["id"]

    name = models.CharField(max_length=200, default="",
                            verbose_name="Название продукта")
    barcode = models.CharField(
        max_length=200, default="", verbose_name="Штрихкод")
    cost = models.FloatField(default=0.0, verbose_name="Стоимость")
    quantity = models.IntegerField(default=0, verbose_name="Количество")
    shelf_life = models.DateField(blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True)
    multiplicity = models.IntegerField(default=1, verbose_name="Кратность", blank=True, null=True)
    pub_date = models.DateTimeField(auto_now=True, verbose_name="Дата прайса", null=True)
    provider = models.ForeignKey(
        Provider, verbose_name="Поставщик", blank=True, null=True)

    def get_status(self, min_date: str) -> bool:
        try:
            if datetime.now().date() + timedelta(days=int(min_date) * 365 / 12) < self.shelf_life:
                return True
            else:
                return False
        except TypeError:
            return False

    def __str__(self):
        return "{0}---{1}".format(self.name, self.provider)

    def format_shelf_life(self) -> str:
        try:
            return f"{self.shelf_life:%d.%m.%Y}"
        except TypeError:
            return "Нет срока годности"

    def format_pub_date(self) -> str:
        try:
            date = self.pub_date + timedelta(hours=8)
            return f"{date:%d.%m.%Y;%H:%M}"
        except TypeError:
            return "Дата выгрзуки не установлена"

    def get_mult(self) -> int:
        if self.multiplicity:
            return self.multiplicity
        else:
            return 1


class OrderProduct(models.Model):
    name = models.CharField(max_length=200, default="",
                            verbose_name="Название продукта")
    barcode = models.CharField(
        max_length=200, default="", verbose_name="Штрихкод")
    cost = models.FloatField(default=0.0, verbose_name="Стоимость")
    quantity = models.IntegerField(default=0, verbose_name="Количество")

    order = models.ForeignKey(
        Order, blank=True, verbose_name="Заказ", null=True)
    provider = models.ForeignKey(
        Provider, verbose_name="Поставщик", blank=True, null=True)

    def __str__(self):
        return "{0}---{1}".format(self.name, self.provider.id)

    def get_summary(self) -> str:
        summary = self.cost * self.quantity
        return "%.2f" % summary
