from django.views.generic import ListView
from django.http import HttpResponse

from main.models import Provider


class SettingsView(ListView):
    model = Provider
    template_name = "settings.html"

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context["user_providers"] = Provider.objects.all().filter(user=self.request.user)
        print(context["user_providers"])
        context["workers_user"] = Provider.objects.all().filter(work_with=self.request.user)
        context["min_date"] = self.request.session["min_date"]
        context["show"] = self.request.session["show"]
        if self.request.session.get("min_sums") is None:
            min_sums = {}
            self.request.session["min_sums"] = min_sums
        else:
            min_sums = self.request.session["min_sums"]
        context["min_sums"] = min_sums
        context["title"] = "settings"
        return context


def date_filters(request):
    request.session["min_date"] = request.GET.get("time")
    if request.GET.get("show") == "false":
        request.session["show"] = "false"
    else:
        request.session["show"] = "true"
    return HttpResponse("successful")


def attach_to_user(request):
    provider = Provider.objects.get(id=int(request.GET.get("provider")))
    provider.work_with.add(request.user)
    provider.save()
    return HttpResponse("successful")


def detach_from_user(request):
    provider = Provider.objects.get(id=int(request.GET.get("provider")))
    provider.work_with.remove(request.user)
    provider.save()
    return HttpResponse("successful")


def change_min_order(request):
    min_sums = request.session["min_sums"]
    provider = Provider.objects.get(id=int(request.GET["provider"]))
    if request.GET["min_sum"] != "":
        min_sums[str(provider)] = request.GET["min_sum"]
    else:
        min_sums[request.GET["provider"]] = provider.min_order
    request.session["min_sums"] = min_sums
    print(min_sums)
    return HttpResponse("successful")
