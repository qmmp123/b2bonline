from django import template

register = template.Library()

@register.filter(name="get_item")
def get_item(dictionary, key):
	if dictionary.get(str(key)):
		return dictionary.get(key)
	else:
		return 0

register.filter("get_item", get_item)